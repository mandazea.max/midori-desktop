import Midori
import unittest

class BasicTest(unittest.TestCase):

    def test_module_version(self):
        self.assertEqual(Midori.__version__.count('.'), 2)
        self.assertIsNotNone(Midori.registerPlugin)

    def test_mainapplication(self):
        self.assertIsNotNone(Midori.MainApplication.instance())

    def test_create_window(self):
        window = Midori.MainApplication.instance().createWindow(Midori.Qz.BW_NewWindow)
        self.assertIsNotNone(window)

    def test_sql_availability(self):
        self.assertTrue(hasattr(Midori.SqlDatabase, 'database'))


suite = unittest.defaultTestLoader.loadTestsFromTestCase(BasicTest)
if unittest.TextTestRunner().run(suite).failures:
    raise(Exception("FAIL"))
