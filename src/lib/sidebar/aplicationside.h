#ifndef APLICATIONSIDE_H
#define APLICATIONSIDE_H

#include <QWidget>

namespace Ui {
class aplicationside;
}

class aplicationside : public QWidget
{
    Q_OBJECT

public:
    explicit aplicationside(QWidget *parent = nullptr);
    ~aplicationside();

private:
    Ui::aplicationside *ui;
};

#endif // APLICATIONSIDE_H
