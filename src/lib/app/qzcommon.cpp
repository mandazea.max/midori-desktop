/* ============================================================
* Falkon - Qt web browser
* Copyright (C) 2014-2018 David Rosca <nowrep@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* ============================================================ */
#include "qzcommon.h"
#include "../config.h"

namespace Qz
{
const int sessionVersion = 0x0004;

MIDORI_EXPORT const char *APPNAME = "Midori Browser";
MIDORI_EXPORT const char *VERSION = MIDORI_VERSION;
MIDORI_EXPORT const char *AUTHOR = "David Rosca";
MIDORI_EXPORT const char *COPYRIGHT = "2010-2018";
MIDORI_EXPORT const char *WWWADDRESS = "https://astian.org/midori-browser";
MIDORI_EXPORT const char *BUGSADDRESS = "https://gitlab.com/midori-web/midori-desktop/-/issues";
MIDORI_EXPORT const char *WIKIADDRESS = "https://gitlab.com/midori-web/midori-desktop/-/wikis/home";
}
